<?php
/**
 * Yada child theme functions and definitions.
 *
 * Add your custom PHP in this file.
 * Only edit this file if you have direct access to it on your server (to fix errors if they happen).
 */

function yada_child_enqueue_scripts() {
	if ( is_rtl() ) {
		wp_enqueue_style( 'generatepress-rtl', trailingslashit( get_template_directory_uri() ) . 'rtl.css' );
	}
}

add_action( 'wp_enqueue_scripts', 'yada_child_enqueue_scripts', 100 );


if ( ! function_exists( 'generate_setup' ) ) {
	add_action( 'after_setup_theme', 'generate_setup' );
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * @since 0.1
	 */
	function generate_setup() {
		// Make theme available for translation.
		load_child_theme_textdomain( 'yada', get_stylesheet_directory() . '/languages' );
		
		add_action( 'wp_loaded', 'remove_my_parent_theme_function' );
		function remove_my_parent_theme_function() {
			remove_action( 'after_setup_theme', 'generate_article_schema' );
			remove_action( 'after_setup_theme', 'generate_body_itemtype' );
			remove_action( 'after_setup_theme', 'generate_clone_sidebar_navigation' );
		}
        
		// @param page slug
		// @param post type
		function the_slug_exists( $post_name, $post_type ) {
			get_posts( array( 'fields'      => 'ids',
			                  'numberposts' => 1,
			                  'post_name'   => $post_name,
			                  'post_type'   => $post_type
			) );
		}

        // create the about page
		if ( isset( $_GET['activated'] ) && is_admin() ) {
			$about_page_title = 'About ' . get_field( 'brand_name', 'option' );
			$about_page_check = get_page_by_title( $about_page_title );
			$about_page       = array(
				'post_type'   => 'page',
				'post_title'  => $about_page_title,
				'post_status' => 'publish',
				'post_author' => 1,
				'post_slug'   => 'about'
			);
			if ( ! isset( $about_page_check->ID ) && ! the_slug_exists( $about_page[ 'post_slug' ], $about_page[ 'post_type' ] ) ) {
				$about_page_id = wp_insert_post( $about_page );
			}
		}

	// Add theme support for various features.
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'post-formats', array( 'aside', 'chat', 'image', 'video', 'quote', 'link', 'status' ) );
	add_theme_support( 'woocommerce' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );
	add_theme_support( 'customize-selective-refresh-widgets' );

	// Register primary menu.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'yada' ),
	) );

	add_theme_support( 'custom-logo', array(
		'height'      => 70,
		'width'       => 350,
		'flex-height' => true,
		'flex-width'  => true
	) );
}

function yada_breadcrumb() {
	$category = get_the_category();
	$cat_name = $category[0]->cat_name;
	$cat_link = get_category_link( $category[0]->term_id );

	echo '<nav class="breadcrumb"><a href="' . get_site_url() . '" title="' . __( 'Home', 'yada' ) . '">' . __( 'Home', 'yada' ) . '</a> &rarrtl; <a href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a> &rarrtl; <strong>' . get_the_title() . '</strong></nav>';
}

//require get_stylesheet_directory() . '/lib/php/acf-functions.php';
require_once get_stylesheet_directory() . '/lib/php/class-tgm-plugin-activation.php';
require get_stylesheet_directory() . '/inc/navigation.php';
require get_stylesheet_directory() . '/inc/generate_posted_on.php';
require get_stylesheet_directory() . '/inc/generate_construct_header.php';
require get_stylesheet_directory() . '/inc/generate_construct_site_title.php';
require get_stylesheet_directory() . '/inc/featured-image.php';
require get_stylesheet_directory() . '/inc/generate_add_footer_info.php';
//require get_stylesheet_directory() . '/lib/php/acf-storage.php';
require get_stylesheet_directory() . '/lib/php/tgmpa_init.php';
require get_stylesheet_directory() . '/inc/breadcrumbs-json.php';

add_action( 'acf/init', 'my_acf_init' );

function my_acf_init() {
	acf_update_setting( 'google_api_key', get_field('google_maps_api', 'option') );
	if ( function_exists( 'acf_add_options_page' ) ) {

		$option_page = acf_add_options_page( array(
			'page_title' => __( 'Organization Data', 'yada' ),
			'menu_title' => __( 'Schema.org', 'yada' ),
			'menu_slug'  => 'organization-data',
			/* (int|string) The position in the menu order this menu should appear.
	WARNING: if two menu items use the same position attribute, one of the items may be overwritten so that only one item displays!
	Risk of conflict can be reduced by using decimal instead of integer values, e.g. '63.3' instead of 63 (must use quotes).
	Defaults to bottom of utility menu items */
			'position'   => 59.75,
			'icon_url'   => 'dashicons-groups'
		) );
	}
}
};

function add_taxonomies_to_pages() {
	register_taxonomy_for_object_type( 'post_tag', 'page' );
	register_taxonomy_for_object_type( 'category', 'page' );
}

add_action( 'init', 'add_taxonomies_to_pages' );
if ( ! is_admin() ) {
	add_action( 'pre_get_posts', 'category_and_tag_archives' );
}

function category_and_tag_archives( $wp_query ) {
	$my_post_array = array( 'post', 'page' );

	if ( $wp_query->get( 'category_name' ) || $wp_query->get( 'cat' ) ) {
		$wp_query->set( 'post_type', $my_post_array );
	}

	if ( $wp_query->get( 'tag' ) ) {
		$wp_query->set( 'post_type', $my_post_array );
	}
}

function comma_tags( $tags, $show_links = true ) {
	if ( $tags ) {
		$t = array();
		foreach ( $tags as $tag ) {
			$t[] = ( ! $show_links ) ? $tag->name : '{"@type": "Thing", "@id": "' . get_tag_link( $tag->term_id ) . '", "name": "' . $tag->name . '"}';
		}

		return implode( ", ", $t );
	} else {
		return false;
	}
}

/*
Yoast SEO JSON+LD Disable
*/
    add_filter('wpseo_json_ld_output', '__return_false');
