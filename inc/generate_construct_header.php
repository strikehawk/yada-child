<?php
if ( ! function_exists( 'generate_construct_header' ) ) {
	add_action( 'generate_header', 'generate_construct_header' );
	/**
	 * Build the header.
	 *
	 * @since 1.3.42
	 */
	function generate_construct_header() {
		?>
		<header id="masthead" <?php generate_header_class(); ?>>
			<div <?php generate_inside_header_class(); ?>>
				<?php
				/**
				 * generate_before_header_content hook.
				 *
				 * @since 0.1
				 */
				do_action( 'generate_before_header_content' );

				// Add our main header items.
				generate_header_items();

				/**
				 * generate_after_header_content hook.
				 *
				 * @since 0.1
				 *
				 * @hooked generate_add_navigation_float_right - 5
				 */
				do_action( 'generate_after_header_content' );
				?>
			</div><!-- .inside-header -->
		</header><!-- #masthead -->
		<?php
	}
}
