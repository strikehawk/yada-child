<?php
// Initialise this action
add_action('wp_head', 'json_ld_about');

function json_ld_about() {
  // Only on single pages
  if ( is_page_template( 'page-templates/about-page.php' ) )
  {
    // Set POST data into memory
    global $post;
    setup_postdata($post);
    // Variables
    $custom_logo_id = get_theme_mod( 'custom_logo' );
    $logo = wp_prepare_attachment_for_js($custom_logo_id);
    $image = get_post_thumbnail_id(($post->ID), 'full');
    $attachment_meta = wp_prepare_attachment_for_js($image);
    $excerpt = get_the_excerpt();
    $breadcrumb[] = wp_schema_breadcrumbs();
    $words = str_word_count(strip_tags($post->post_content), 0);

   $html = array(
    '@context' => "https://schema.org",
       '@type' => get_field('schema_type', 'options'),
            'logo'  => array(
                '@type' => "ImageObject",
                    'url' => $logo[ 'url' ],
                    'height' => $logo[ 'height' ],
                    'width' => $logo[ 'width' ],
                    'name' => $logo[ 'alt' ],
                    'description' => $logo[ 'description' ]
            ),
            'image'  => array(
                '@type' => "ImageObject",
                    'name' => $attachment_meta[ 'alt' ],
                    'url' => $attachment_meta[ 'url'],
                    'height' => $attachment_meta[ 'height' ],
                    'width' => $attachment_meta[ 'width' ],
                    'description' => $attachment_meta[ 'description' ],
            ),
            '@id' => "#organization",
            'name' => get_the_title(),
            'legalName' => get_field('legal_name', 'option'),
            'description' => strip_tags(get_field('brand_description', 'option')),
            'datePublished' => get_the_date('c'),
            'dateModified' => get_the_modified_date('c'),
            'telephone' => get_field('company_phone', 'options'),
            'address'   => array(
                '@type'           => 'PostalAddress',
                'streetAddress'   => get_field('address_street', 'option'),
                'postalCode'      => get_field('address_postal', 'option'),
                'addressLocality' => get_field('address_locality', 'option'),
                'addressRegion'   => get_field('address_region', 'option'),
                'addressCountry'  => get_field('address_country', 'option')
            )
    );
        // OPENING HOURS
        if (have_rows('opening_hours', 'option')) {
            $html['openingHoursSpecification'] = array();
            while (have_rows('opening_hours', 'option')) : the_row();
                $closed = get_sub_field('closed');
                $from   = $closed ? '00:00' : get_sub_field('from');
                $to     = $closed ? '00:00' : get_sub_field('to');
                $openings = array(
                    '@type'     => 'OpeningHoursSpecification',
                    'dayOfWeek' => get_sub_field('days'),
                    'opens'     => $from,
                    'closes'    => $to
                );
        array_push($html['openingHoursSpecification'], $openings);
        endwhile;
        /// VACATIONS / HOLIDAYS
        if (have_rows('special_days', 'option')) {
            while (have_rows('special_days', 'option')) : the_row();
                $closed    = get_sub_field('closed');
                $date_from = get_sub_field('date_from');
                $date_to   = get_sub_field('date_to');
                $time_from = $closed ? '00:00' : get_sub_field('time_from');
                $time_to   = $closed ? '00:00' : get_sub_field('time_to');
                $special_days = array(
                    '@type'        => 'OpeningHoursSpecification',
                    'validFrom'    => $date_from,
                    'validThrough' => $date_to,
                    'opens'        => $time_from,
                    'closes'       => $time_to
                );
            array_push( $html[ 'openingHoursSpecification' ], $special_days );
            endwhile;
        }
    }
    /// CONTACT POINTS
    if (get_field('contact', 'options')) {
        $html['contactPoint'] = array();
        while (have_rows('contact', 'options')) : the_row();
            $contacts = array(
                '@type'       => 'ContactPoint',
                'contactType' => get_sub_field('type'),
                'telephone'   => get_sub_field('phone')
            );
            if (get_sub_field('option')) {
                $contacts['contactOption'] = get_sub_field('option');
            }
            array_push($html['contactPoint'], $contacts);
        endwhile;
    }
    array_push($html, array(
        'mainEntityOfPage' => array(
            '@type' => "AboutPage",
            '@id' => get_the_permalink(),
            'potentialAction' => array(
                '@type' => "SearchAction",
                'target' => site_url() .'/search?q={search_term_string}',
                'query-input' => "required name=search_term_string"
            ),
            'name' => get_the_title(),
            'keywords' => comma_tags( get_the_tags(), false ),
            'mentions' => "[".comma_tags( get_the_tags(), true )."]",
            'breadcrumb' => array_push ($breadcrumb),
            'image' => array(
                '@type' => "ImageObject",
                    'url' => $attachment_meta[ 'url' ],
                    'height' => $attachment_meta[ 'height' ],
                    'width' => $attachment_meta[ 'width' ],
                    'name' => $attachment_meta[ 'alt' ],
                    'description' => $attachment_meta[ 'description' ],
                    'representativeOfPage' => "true",
            ),
            'lastReviewed' => get_the_modified_date('c'),
            'reviewedBy' => array(
                '@type' => 'Person',
                'name' => get_the_author(),
            ),
        ),
        'author' => array(
            '@type' => 'person',
            'name' => get_the_author(),
        ),
        'publisher' => array(
          '@id' =>' '#organization'
        ),
    ));

echo '<script type="application/ld+json">' . json_encode($html) . '</script>' . "\r\n";
    };
};
