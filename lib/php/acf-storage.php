<?php 

// variables
$plugin = 'plugins/advanced-custom-fields-pro/acf.php';
$storage = wp_upload_dir('', true, true );
$storage = $storage[ 'basedir' ] . '/acf-json';

if ( is_plugin_active( $plugin ) ) {

function my_acf_init() {
	
	acf_update_setting('save_json', $storage );
	
}

add_action('acf/init', 'my_acf_init');


/**
 * Fix location of ACF local JSON.
 *
 * Since Sage does some surgery on the WordPress template locations, ACF looks in
 * the wrong location for the acf-json directory. We will fix this by manually
 * hooking into that functionality and attempting to save in the right spot.
 *
 * @param  string  $path
 * @return string
 */
add_filter('acf/settings/save_json', function ($path) {
    return (file_exists($storage) && is_dir($storage)) ? $storage : $path;
});

add_filter('acf/settings/load_json', 'my_acf_json_load_point');

    function my_acf_json_load_point( $paths ) {
    
    // remove original path (optional)
        unset($paths[0]);
    
    
    // append path
        $paths[] = $storage;
    
    
    // return
        return $paths;
    
    }
}