Organizing Theme Files
https://developer.wordpress.org/themes/basics/organizing-theme-files/

While WordPress themes technically only require two files (index.php and style.css), they usually are made up of many files. That means they can quickly become disorganized! This section will show you how to keep your files organized.
Note: Themes without header.php and footer.php, with no alternative available, have been deprecated since WordPress 3.0. Your theme may need to include these files as well.

Theme folder and file structure #Theme folder and file structure
As mentioned previously, the default Twenty themes are some of the best examples of good theme development. For instance, here is how the Twenty Seventeen Theme organizes its file structure:

```
assets (dir)
      - css (dir)
      - images (dir)
      - js (dir)
inc (dir)
template-parts (dir)
      - footer (dir)
      - header (dir)
      - navigation (dir)
      - page (dir)
      - post (dir)
404.php
archive.php
comments.php
footer.php
front-page.php
functions.php
header.php
index.php
page.php
README.txt
rtl.css
screenshot.png
search.php
searchform.php
sidebar.php
single.php
style.css
```

You can see that the main theme template files are in the root directory, while JavaScript, CSS, images are placed in assets directory, template-parts are placed in under respective subdirectory of template-parts and collection of  functions related to core functionalities are placed in inc directory.

At this time there are no required folders within a WordPress theme. However, WordPress does recognize the following folders by default.